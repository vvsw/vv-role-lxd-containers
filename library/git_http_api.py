# for this module, correct error handling is wip, ie: verbose=True will make task fail due to junk
# verivina 2020

from ansible.module_utils.basic import AnsibleModule
import urllib.parse
import requests
import os, shutil


ENABLE_UNIT_TESTS=False

class DirectoryListAPI:
    def _debug( self, s ):
        if self.verbose:
            print("[DEBUG]: " + str(s))

    def error( self, s):
        raise ValueError(s)

    def __init__(self, root="", verbose=True):
        assert root != ""
        self.root = os.path.join(root)
        self.verbose = verbose

    def os_list_directory(self,  path=""):
        path = os.path.join(self.root, path)
        if not os.path.isdir(path):
            return None
        return os.listdir(path) 

    def os_get_sha(self):
        pass

    def os_get_content(self, path=""):
        path = os.path.join(self.root, path)
        if not os.path.isfile(path):
            return None
        ret = ""
        with open(path) as f:
            ret = f.read()
        return ret


class gitContentAPI:
    def _debug( self, s ):
        if self.verbose:
            print("[DEBUG]: " + str(s))

    def error( self, s):
        raise ValueError(s)

    def __init__(self, gitlab_project_id, verbose = False, access_token = "", branch = "master"):
        self.verbose = verbose
        self.gitlab_project_id = str(gitlab_project_id)
        self.access_token = access_token
        self.branch = urllib.parse.quote(branch, safe='')
        self.gitlab_api_url = 'https://gitlab.com/api/v4/projects/' + self.gitlab_project_id
        self.header = { 'Private-Token': self.access_token }
        if( gitlab_project_id == None):
            self.error("[ERROR]: access token not defined\n")

        self.verbose = verbose

    def git_list_directory(self,  path="/" ):
        url = self.gitlab_api_url + '/repository/tree?ref=' + self.branch + "&path=" + urllib.parse.quote(path)
        self._debug( "url=" + url )
        r = requests.get( url, headers=self.header )
        if( r.status_code != 200 ):
            self.error("[ERROR]: git api returned non 200 status code.\n" + r.text )
        response = r.json()
        self._debug(response)
        ret_dir_list = []
        for item in response:
            ret_dir_list = ret_dir_list + [ item["name"] ]
        self._debug(ret_dir_list)
        return ret_dir_list

    def git_get_commit(self):

        # if path != "/":
        #     path = path.replace("/", "%2F")
        #     filename = path + "%2F" + filename
        url = self.gitlab_api_url + '/repository/commits/' + self.branch 
        self._debug( "url=" + url )
        r = requests.get( url, headers=self.header )
        if( r.status_code != 200 ):
            self.error("[ERROR]: git api returned non 200 status code.\n" + r.text )
        self._debug(r.json())
        return r.json()["id"]

    def git_get_content(self, path):
        # if path != "/":
        #     path = path.replace("/", "%2F")
        #     filename = path + "%2F" + filename
        url = self.gitlab_api_url + '/repository/files/' + path.replace('/',"%2F") + '/raw?ref=' + self.branch 
        self._debug( "url=" + url )
        r = requests.get( url, headers=self.header )
        if( r.status_code != 200 ):
            self.error("[ERROR]: git api returned non 200 status code.\n" + r.text )
        self._debug(r.text)
        return r.text

def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        op=dict(type='str', required=True),
        root=dict(type='str', required=True),
        access_token=dict(type='str', required=False, default=""),
        gitlab_project_id=dict(type='str', required=True),
        path=dict(type='str', required=False, default=""),
        verbose=dict(type='bool', required=False, default=False),
        ref=dict(type='str', required=False, default="master"),
        source=dict(type='str', required=False, default="git")
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        data={},
        success=False,
        module_args=module_args
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    if 'source' not in module.params or module.params["source"] == 'git':
        assert(module.params["gitlab_project_id"] != "")
        git = gitContentAPI(module.params['gitlab_project_id'], access_token=module.params["access_token"], branch=module.params['ref'])
        if(module.params['verbose']):
            git.verbose = True
        commit = git.git_get_commit()
        result['commit'] = commit

    # read in stack information via os.file
    elif module.params["source"] == 'fs':
        # if root path does not exist on fs, fail the module
        if not os.path.isdir(module.params['root']):
            result['failed'] = True
            result['msg'] = f'Directory={module.params["root"]} does not exist'
            module.exit_json(**result)

        fs = DirectoryListAPI(root=module.params['root'])
        if(module.params['verbose']):
            fs.verbose = True
        if 'path' in module.params:
            path = os.path.join(module.params['path'])
        else:
            path = ''

    if module.params['op'] == 'ls':
        if module.params["source"] == 'git':
            (folders) = git.git_list_directory(path=module.params['path'])

        elif module.params["source"] == 'fs':
            folders = fs.os_list_directory(path=path)
        result['data'] = folders
        result['success'] = True
        module.exit_json(**result)

    elif module.params['op'] == 'cat':
        if module.params["source"] == 'git':
            (content) = git.git_get_content(path=module.params['path'])
        elif module.params["source"] == 'fs':
            content = fs.os_get_content(path=path)
        assert(type(content) == str)
        result['data'] = content
        result['success'] = True
        module.exit_json(**result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


def test_directory_list_api():

    test_directory_path = os.path.join("/tmp", "test_a_b_c")
    if os.path.isdir(test_directory_path):
        shutil.rmtree(test_directory_path)
    test_subdirectory_path_a = os.path.join(test_directory_path, "a")
    test_subdirectory_path_b = os.path.join(test_directory_path, "b")
    test_file_path = os.path.join(test_subdirectory_path_a, "test.yml")
    test_file_content = "test1 \n test2 \n test3"

    os.mkdir(test_directory_path)
    os.mkdir(test_subdirectory_path_a)
    os.mkdir(test_subdirectory_path_b)
    with open(test_file_path, "w+") as f:
        f.write(test_file_content)

    d = DirectoryListAPI(root="/tmp")

    t1 = d.os_list_directory(test_directory_path)
    assert(t1 == ['a','b'])

    f1 = d.os_get_content(test_file_path)
    assert(f1 == test_file_content)
    
    assert d.os_list_directory("/tmp2/test/fake") == None
    
    shutil.rmtree(test_directory_path)

    print("tests passed ☕")


def test_git_http_api():
    # g = gitContentAPI("20820467", branch="65e12a897ced293c10187252502530a68d5a5ce9")
    # g.verbose = True

    # commit = g.git_get_commit()
    # assert(commit == '65e12a897ced293c10187252502530a68d5a5ce9')
    # assert((g.git_list_directory("containers")))

    g = gitContentAPI("24821010", branch="molecule/subnet_of_12.0.0.x_cement")
    commit = g.git_get_commit()
    print("commit={}".format(commit))
    print("contents of '../containers/={}'".format(g.git_list_directory("containers")))
    test_data_containers_dir_content = ['us-numbersro-char', 'us-numbersro-login', 'us-numbersro-map', 'us-numbersro-mariadb-log-db', 'us-numbersro-mariadb']
    assert( g.git_list_directory("containers") == test_data_containers_dir_content )
    assert(commit == '979f257ea4f47ea13370d40ed4ed574abedc603d')    
    print("tests passed ☕")
    

def test():
    test_directory_list_api()
    test_git_http_api()

def main():
    test()
    #run_module()


if __name__ == '__main__':
    if ENABLE_UNIT_TESTS:
        test()
    else:
        run_module()
    #run_module()
