# vv-role-lxd-containers

vv-role-lxd-containers: 

- creates and bootstraps containers specified in vv-lxd-container repository
- bootstraps the container with neccessary authorized_keys, users, tunnel, init script, etc.

`molecule test command:` - `ANSIBLE_VAULT_PASSWORD_FILE=molecule/default/.ansible_vault_key molecule converge`
