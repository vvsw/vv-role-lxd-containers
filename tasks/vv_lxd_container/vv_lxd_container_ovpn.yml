# ---
- name: "init network for container={{item.name}}"
  set_fact: vv_container_folder="{{item}}"

- set_fact: 
      vv_lxd_containers_repo_ref: "{{ vv_lxd_non_core_containers_spec_ref | urlencode() }}"

- name: download the specification for container={{item.name}} from vv-lxd-container repo  
  uri:
    url: "https://gitlab.com/api/v4/projects/{{vv_lxd_containers_gitlab_project_id}}/repository/files/{{vv_container_folder.name}}%2Fcontainer.yml/raw?ref={{vv_lxd_containers_repo_ref | urlencode() }}"
    return_content: yes
    method: GET
    headers:
      Private-Token: "{{vv_ansible_gitlab_read_access_token}}"
  register: this_container_yml
  retries: 3
  until: this_container_yml.status == 200

- set_fact:
    this_lxd_container: "{{ this_container_yml.content | from_yaml }}"

- debug: msg="lxd.container.name={{ this_lxd_container.vv_lxd_container_name }}"

- name: "check if tun0 exists inside container={{this_lxd_container.vv_lxd_container_name}}"
  command: "{{bin_lxd_path}}/lxc exec {{this_lxd_container.vv_lxd_container_name}} -- ifconfig"
  register: this_container_ifconfig

- name: "initialize and generate openvpn conf/keys/tun0 for container={{this_lxd_container.vv_lxd_container_name}}"
  block: 
  - name: add vv-lxd-openvpn to device_inventory
    add_host:
      hostname: "{{ remote_lxd_addr }}:{{vv_lxd_openvpn_container_name}}"
      groups:
        - vv_lxd_containers
      ansible_connection: lxd

   # initialize vv-lxd-openvpn core container config for client key generation
  - name: ensure client conf directory exists in vv-lxd-openvpn
    file:
      path: "{{vv_lxd_openvpn_client_conf_directory}}"
      state: directory
    delegate_to: "{{ remote_lxd_addr }}:{{vv_lxd_openvpn_container_name}}"
    become: yes

  - name: check if client ovpn conf already exists
    stat: 
      path: "{{vv_lxd_openvpn_client_conf_directory}}/{{this_lxd_container.vv_lxd_container_name}}.ovpn"
    register: this_lxd_container_ovpn_conf_stat
    delegate_to: "{{ remote_lxd_addr }}:{{vv_lxd_openvpn_container_name}}"

  # load secrets for vv-lxd-openvpn to generate client confs
  

    # - name: debug print out envars
    #   command: "bash -c 'export'"
    #   delegate_to: "{{vv_lxd_openvpn_container_name}}"
    #   become: yes
    #   environment: "{{vv_lxd_openvpn_secrets_env}}"

  - name: create client conf on the gateway openvpn server , vv-lxd-openvpn
    command: "{{vv_lxd_openvpn_client_key_gen_path}} {{this_lxd_container.vv_lxd_container_name}} {{vv_lxd_openvpn_ipv4_port}}"
    delegate_to: "{{ remote_lxd_addr }}:{{vv_lxd_openvpn_container_name}}"
    become: yes
    environment: "{{vv_lxd_openvpn_secrets_env}}"
    when: "this_lxd_container_ovpn_conf_stat.stat.exists == False"

  - name: "add container={{this_lxd_container.vv_lxd_container_name}} to device_inventory"
    add_host:
      hostname: "{{this_lxd_container.vv_lxd_container_name}}"
      groups:
        - vv_lxd_containers
      ansible_connection: lxd

  - name: "install and intialize openvpn inside container={{this_lxd_container.vv_lxd_container_name}}"
    block:
    - name: install openvpn 2.4.x for ubuntu 16.04 xenial distros
      block: 
      - name: add apt-key for updated openvpn repo
        apt_key:
          url: https://swupdate.openvpn.net/repos/repo-public.gpg
          state: present
        register: result
        retries: 3
        until: "result.failed != 'false'"

      - name: add openvpn xenail build repo to sourcelist
        apt_repository:
          repo: deb http://build.openvpn.net/debian/openvpn/stable xenial main
          state: present

      - name: install openvpn client inside container
        apt: 
          name: openvpn=2.4.9-xenial0
          state: present
          update_cache: yes
      when: "'xenial' in this_lxd_container.vv_lxd_container_source_alias"

    - name: install latest openvpn for non-xenial distros
      apt:
        name: openvpn
        state: latest
      when: "'xenial' not in this_lxd_container.vv_lxd_container_source_alias"

    - name: set autostart on boot for openvpn
      command: "sed -i '/^#.*all/s/^#//' /etc/default/openvpn"

    # - name: disable NPROC limit, required for lxd ovpn client
    #   command: "sed -i '/LimitNPROC=10/s/^/#/' /lib/systemd/system/openvpn@.service"
    
    # - name: reload systemctl daemon for openvpn service changes to take effect
    #   command: systemctl daemon-reload
    delegate_to: "{{ remote_lxd_addr }}:{{this_lxd_container.vv_lxd_container_name}}"

  - name: copy over the client conf to control machine
    command: "{{bin_lxd_path}}/lxc file pull {{vv_lxd_openvpn_container_name}}/{{vv_lxd_openvpn_client_conf_directory}}/{{this_lxd_container.vv_lxd_container_name}}.ovpn /tmp/{{this_lxd_container.vv_lxd_container_name}}.ovpn"
    delegate_to: localhost

  - name: push the client conf to client container 
    command: "{{bin_lxd_path}}/lxc file push /tmp/{{this_lxd_container.vv_lxd_container_name}}.ovpn {{this_lxd_container.vv_lxd_container_name}}/{{vv_lxd_container_ovpn_client_conf_path}}"
    delegate_to: localhost

  - name:  create cron job to start on boot 
    cron:
      name: "openvpn client start on boot"
      special_time: reboot
      job: "/usr/sbin/openvpn --config {{vv_lxd_container_ovpn_client_conf_path}}"
    delegate_to: "{{ remote_lxd_addr }}:{{this_lxd_container.vv_lxd_container_name}}"
   
  - name: restart containers via lxc for ovpn changes to take effect
    command: "{{bin_lxd_path}}/lxc restart {{this_lxd_container.vv_lxd_container_name}}"
    delegate_to: localhost

  - name: delete ovpn conf copied over to control machine 
    file:
      path: "/tmp/{{this_lxd_container.vv_lxd_container_name}}.ovpn"
      state: absent
  when: "'tun0' not in this_container_ifconfig.stdout"

